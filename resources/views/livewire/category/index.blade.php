<div>
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
</div>
<table class="table table-bordered" style="margin-top:20px;">
    <tr>
        <td>NO</td>
        <td>ID</td>
        <td>NAME</td>
        <td>STATUS</td>
        <td>ACTION</td>
    </tr>
    @foreach($categories as $row)
        <tr>
            <td>{{$loop->index + 1}}</td>
            <td>{{$row->id}}</td>
            <td>{{$row->name}}</td>
            <td>{{$row->active}}</td>
            <td>
                <a href='{{ url("categories/{$row->id}/edit") }}' class="btn btn-primary"><i class="fa fa-edit"></i>Edit</a>
                |
                <button wire:click="destroy({{$row->id}})" class="btn btn-sm btn-outline-danger py-0">Delete</button>
            </td>
        </tr>
    @endforeach
</table>






