<div class="container">
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <form wire:submit.prevent="submit">
        <div class="form-group">
            <label for="name">Category Name: </label>
            <input wire:model="category.name" type="text" name="name" id="name"
                   class="form-control" placeholder="Category name">
            @error('category.name') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="form-group">
            <label for="active">Status</label>
            <input wire:model="category.active" type="text" name="active" id="active"
                   class="form-control" placeholder="Status">
            @error('category.active') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>













