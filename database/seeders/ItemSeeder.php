<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    public function run()
    {
        Item::factory()
            ->count(5)
            ->for(Category::inRandomOrder()->first())
            ->create();
    }
}
