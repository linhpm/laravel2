<?php

namespace App\Http\Livewire\Category;

use App\Models\Category;
use Livewire\Component;

class Edit extends Component
{
    public $category;
    public $success = false;

    public function mount($id)
    {
        $this->category = Category::find($id);
        if (!$this->category) {
            $this->category = new Category();
        }
    }
    public function render()
    {
        return view('livewire.category.edit', ['category'=> $this->category]);
            // ->extends('layouts.app') // using this syntax in case your layout use tradition Blade
            // ->section('content'); // using section in case tradition blade $yield('content')
            // ->layout('layouts.app'); // using layout by default locate at config/livewire.php
            // ->slot('main'); // specific slot in case have multiple slot on the layout
    }

    protected $name, $status;
    protected $rules = [
        'category.name' => 'required|string|max:20',
        'category.active' => 'required|boolean'
    ];
    public function submit()
    {
        $dataValidate = $this->validate();
        // The process break here if the validate is not pass

        if ($this->category->id){ // Update category
            $this->category->update($dataValidate['category']);
            session()->flash('message', __('Post successfully updated.'));
        } else {// Create new category
            $this->category = Category::create($dataValidate['category']);
            session()->flash('message', __('Post successfully created.'));
        }
        $this->redirectRoute('categories.index');
    }
}
