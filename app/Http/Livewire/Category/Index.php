<?php
namespace App\Http\Livewire\Category;
use App\Models\Category;
use Livewire\Component;
class Index extends Component
{
    public $categories;

    public function mount($categories)
    {
        $this->categories = $categories;
    }
    public function render()
    {
        return view('livewire.category.index');
    }

    public function destroy($id)
    {
        if ($id) {
            $record = Category::where('id', $id);
            $record->delete();
            $this->categories->except($id);
        }
    }
}
