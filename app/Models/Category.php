<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public function getListCategory(){
        return Category::with('items')
//            ->where('active', true)
            ->get();
    }


    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'active' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'active',
    ];

    public function items()
    {
        return $this->hasMany(Item::class);
    }


}
